#pragma once

//#include <vector>
//#include <algorithm>
#include "maplen.h"
#include "dynInitaliser.h"
#include "piece.h"
#include "defpieces.h"
#include <vector>
using namespace std;

class player
{
public:
	player();
	player(int ind);
	~player();
	void addPiece(piece addthis);
	int getRemainingCash();  //return current movementcash
	void movePiece(int i, coord newCoord);
	void deductMovementCash(int movementamnt);
	bool canMoveThisFar(int movementamnt);
	void EndTurn();
	void initialisepieces();
protected:
	int index = 0;
	vector<piece> playerPieces;
	int NoPieces = 0;
	void initaliseArrs();
	void delArrs();
	int movementCash = 10; //10 by default
private:
	bool initalised = false;
	int selectedPiece = 0;
	int NormMovementcash = 10;
};

#include "ui.h"

using namespace std;

void ui::print_matrix(bool ** mat, int width, int height)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			if (mat[i][j] == false)
			{
				cout << 0 << " ";
			}
			else
			{
				cout << 1 << " ";
			}
		}
		cout << endl;
	}
}

void ui::print_matrixSel(bool ** mat, int width, int height, int x, int y, int col)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			if (i == x && y == j)
			{
				setConsoleColour(col);
			}
			cout << mat[i][j] << " ";
			if (i == x && y == j)
			{
				defColour();
			}
		}
		cout << endl;
	}
}

void ui::print_matrix(float ** mat, int width, int height)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			cout << mat[i][j] << " ";
		}
		cout << endl;
	}
}

void ui::print_matrix(int ** mat, int width, int height)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			cout << mat[i][j] << " ";
		}
		cout << endl;
	}
}

void ui::printCoord(coord c)
{
	cout << "(" << c.x << "," << c.y << ")";
}

void ui::findColours()
{
	for (int i = 0; i < 255; i++)
	{
		setConsoleColour(i);
		writeLine(i);
	}

	setConsoleColour(BlacknWhite);
}

void ui::setConsoleColour(int colri)
{
	HANDLE  hConsole;
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	FlushConsoleInputBuffer(hConsole);
	SetConsoleTextAttribute(hConsole, colri);
}

void ui::defColour()
{
	setConsoleColour(BlacknWhite);
}

void ui::writeLine(int out)
{
	cout << out << endl;
}

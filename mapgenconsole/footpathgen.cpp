#include "footpathgen.h"

footpathgen::~footpathgen()
{
}

void footpathgen::establishFootpaths(bool ** footpaths, bool ** walkable, int ** blockTypes)
{
	//get 2 points at other ends of the map
	/* initialize random seed: */
	srand(time(NULL));
	quickgrid q = quickgrid();
	vector<coord> path;

	int sx = 0;
	int sy = 0;
	int ex = 0;
	int ey = 0;

	//for the first half
	int i = 0;//index at 0
	int size = 0;
	while (i < attemptFootpathLim)
	{
		getRandomNode(0, sx, sy, 3);
		getRandomNode(2, ex, ey, 3);
		path = q.mainPath(w, h, walkable, sx, sy, ex, ey, size);
		ConnectPoints(footpaths, walkable, blockTypes, path);
		if (size > 2) //consider it a success
			break;
		i += 1;//increase index
	}
	if (size < 3)//failsafe
	{
		getRandomNode(0, sx, sy, 3);
		getRandomNode(2, ex, ey, 3);
		bool** allwalkable = dynInitaliser::makeBool(w, h, true);
		path = q.mainPath(w, h, allwalkable, sx, sy, ex, ey, size);
		ConnectPoints(footpaths, walkable, blockTypes, path);
		dynInitaliser::del(allwalkable, w);//cleanup please
	}
	
	vector<coord> path2;
	int mainsize = size;
	size = 0;
	i = 0;//index at 0 reset
	while (i < attemptFootpathLim)
	{
		//now get random point from a path
		int lowerlim = (mainsize / 2) - 2;
		if (lowerlim < 1)
			lowerlim = 1;
		int nodeind = getrandInt(lowerlim, 6);
		ex = path[nodeind].x;
		ey = path[nodeind].y;
		int select = getrandInt(0, 2);
		int corn = 1;
		if (select == 0)
			corn = 3;
		//now that I have that, get me random node
		getRandomNode(corn, sx, sy, 3);
		path2 = q.mainPath(w, h, walkable, sx, sy, ex, ey, size);
		ConnectPoints(footpaths, walkable, blockTypes, path2);
		if (size > 2) //consider it a success
			break;
		i += 1;//increase index
	}
	if (size < 3)//failsafe
	{
		bool** allwalkable = dynInitaliser::makeBool(w, h, true);
		//now get random point from a path
		int lowerlim = (mainsize / 2) - 2;
		if (lowerlim < 1)
			lowerlim = 1;
		int nodeind = getrandInt(lowerlim, 6);
		ex = path[nodeind].x;
		ey = path[nodeind].y;
		int select = getrandInt(0, 2);
		int corn = 1;
		if (select == 0)
			corn = 3;
		//now that I have that, get me random node
		getRandomNode(corn, sx, sy, 3);
		path2 = q.mainPath(w, h, allwalkable, sx, sy, ex, ey, size);
		ConnectPoints(footpaths, walkable, blockTypes, path2);
		dynInitaliser::del(allwalkable, w);//cleanup please
	}
}

void footpathgen::ConnectPoints(bool ** footpaths, bool ** walkable, int ** blockTypes, vector<coord> path)
{
	int x = 0;
	int y = 0;
	for (int i = 0; i < path.size(); i++)
	{
		x = path[i].x;
		y = path[i].y;
		footpaths[x][y] = true;
		walkable[x][y] = true;
		blockTypes[x][y] = blockTypes::footpath;
	}
}

void footpathgen::getRandomNode(int corner, int & x, int & y, int limitFrom)
{
	switch (corner)
	{
	case 1:
		x = w - 1 - (rand() % limitFrom);
		y = (rand() % limitFrom);
		break;
	case 2:
		x = w - 1 - (rand() % limitFrom);
		y = h - 1 - (rand() % limitFrom);
		break;
	case 3:
		x = (rand() % limitFrom);
		y = h - 1 - (rand() % limitFrom);
		break;
	default://same as 0
		x = (rand() % limitFrom);
		y = (rand() % limitFrom);
	}
}

int footpathgen::getrandInt(int min, int range)
{
	return min + (rand() % range);
}

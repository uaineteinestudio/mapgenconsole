#pragma once

#include "featuregen.h"
#include "coord.h"
#include "boundchecker.h"

class oasisgen : public featuregen
{
public:
	oasisgen(int width, int height, int size) : featuregen(width, height, 1)
	{
		//size of oasis
		radius = size;
		overrideMap();
	}
	~oasisgen();
	coord getSpawnCentreLoc(int** types); //return the centre location for the block types
	void makeOasis(coord makehere, int** types,  int** surfacetypes); //override
	void overrideMap(); //override with new map
protected:
	int radius;
};


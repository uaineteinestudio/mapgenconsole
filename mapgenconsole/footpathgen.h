#pragma once

#include "genclass.h"
#include <vector>
#include "maplen.h"
#include "blockTypes.h"
#include "quickgrid.h"
#include "coord.h"
#include "dynInitaliser.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

class footpathgen : genclass
{
public:
	footpathgen(int width, int height) : genclass(width, height)
	{
		//nothing more to add
	}
	~footpathgen();
	void establishFootpaths(bool** footpaths, bool** walkable, int** blockTypes);
private:
	void ConnectPoints(bool ** footpaths, bool ** walkable, int ** blockTypes, vector<coord> path);
	void getRandomNode(int corner, int &x, int &y, int limitFrom); //get from corner and limit away from that corner
	int getrandInt(int min, int range);
	static const int attemptFootpathLim = 25; //if this is breached go with a footpath that ignores blockades
};
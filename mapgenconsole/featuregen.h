#pragma once

#include "genclass.h"
#include "blockTypes.h"
#include "coord.h"

static const int NoFeatures = 4;
//1 is oasis
//2 is relic site
//3 is cave treasure
//4 is farm fencing walls
//more to come
static const int maxFLen = 7;

class featuregen : public genclass
{
public:
	featuregen() : genclass(MaxLen, MaxLen) 
	{
		fID = 0; //default
		initaliseMap();//calls override map
	};
	featuregen(int width, int height, int featureID) : genclass(width, height)
	{
		//new things
		fID = featureID;
		initaliseMap();
	}
	~featuregen();
	void makeOasis(coord makehere, int** types, int** surfacetypes); //can be overridden
protected:
	int fID; //feature ID
	int map[maxFLen][maxFLen];				//centre given for 7 wide 0 1 2 3 4 5 6
	static const int centCord = maxFLen / 2;//as this making it 3 as the middle
	void overrideMap();
private:
	//make null blocks
	void initaliseMap();
};
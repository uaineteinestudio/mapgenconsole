#include "foliagegen.h"

using namespace std;

foliagegen::~foliagegen()
{
}

void foliagegen::generateFoliage(int ** blocktypes, int ** surfacetypes, float** temp)
{
	//get 2 points at other ends of the map
	/* initialize random seed: */
	srand(time(NULL));

	//make some foliage by 'grassy' areas but perhaps overlap with CA
	//could also consider temperature
	CA ca = CA(0.4, 3, 3, 2);
	bool** foliageArea = ca.cellautomata(w, h);
	float r = 0;
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			if (!foliageArea[x][y])//condition 1
			{
				if (blockTypes::foliageCanGrow(blocktypes[x][y])) //conditions 2
				{
					if (temp[x][y] < foliageTempLim) //another condition 3
					{
						r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX); //get float bet 0 and 1
						if (r < chanceTree)
						{
							makeTree(x, y, surfacetypes);
						}
						else
						{
							r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX); //get float bet 0 and 1
							if (r < foliageChance)
								surfacetypes[x][y] = surfaceTypes::foliage;
							else //gar nichts
							{
								//do nothing
							}
						}
					}
				}
			}
		}
	}
	//del
	dynInitaliser::del(foliageArea, w);
}

void foliagegen::makeTree(int xi, int yi, int ** surfaceMap)
{
	float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX); //get float bet 0 and 1
	if (r < chanceBigTree)
	{
		surfaceMap[xi][yi] = surfaceTypes::Bigtree;
	}
	else
	{
		surfaceMap[xi][yi] = surfaceTypes::tree;
	}
}

void foliagegen::makeFoliage(int xi, int yi, int ** surfaceMap)
{
	surfaceMap[xi][yi] = surfaceTypes::foliage;
}

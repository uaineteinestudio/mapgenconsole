#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

static class mapio
{
public:
	static void writeFile(string fn, int w, int h, int** types, int** stypes, int** heights, int** rot, float** tmp, bool** water, bool** footpaths, bool** caves);
	static void loadFile(string fn, int &w, int &h, int **types, int** stypes, int** heights, int** rot, float** tmp, bool** water, bool** footpaths, bool** caves); //read into all this please
private:
	static void write2DMap(ofstream* myfile, int w, int h, int**map);
	static void write2DMap(ofstream* myfile, int w, int h, bool**map);
	static void write2DMap(ofstream* myfile, int w, int h, float**map);
	static string readLine(ifstream* myfile);
	static bool under10(int val);
	static bool under100(int val);
	static int parseInt(string val);
	static float parseFloat(string val);
	static bool parseBool(string val);
	static void parseIntMap(string line, int **map, int w, int h);
	static void parseBoolMap(string line, bool **map, int w, int h);
};


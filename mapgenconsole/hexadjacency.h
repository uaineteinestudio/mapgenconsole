#pragma once

static const int HexAdjLen = 6;

static const int colMat[2][HexAdjLen] = 
{
	{ -1,  1, 0, 0, -1,  1 },
	{ -1,  0, 1, 1, 0,  -1}
};
static const int rowMat[2][HexAdjLen] =
{
	{ 0,  0,  -1, 1, 1, 1},
	{ 0, 1, 0, -1, -1, -1}
};


static class hexAdjaceny
{
public:
	static void WriteColRow(int* colarr, int* rowarr, int xi, int yi);
};
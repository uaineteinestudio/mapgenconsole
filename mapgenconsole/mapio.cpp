#include "mapio.h"

using namespace std;

void mapio::writeFile(string fn, int w, int h, int** types, int** stypes, int** heights, int** rot, float** tmp, bool** water, bool** footpaths, bool** caves)
{
	ofstream myfile;
	myfile.open(fn);
	myfile << w << "\n";
	myfile << h << "\n";
	write2DMap(&myfile, w, h, types);
	write2DMap(&myfile, w, h, stypes);
	write2DMap(&myfile, w, h, heights);
	write2DMap(&myfile, w, h, rot);
	write2DMap(&myfile, w, h, tmp);
	write2DMap(&myfile, w, h, water);
	write2DMap(&myfile, w, h, footpaths);
	write2DMap(&myfile, w, h, caves);
	myfile.close();
}

void mapio::loadFile(string fn, int &w, int &h, int ** types, int ** stypes, int ** heights, int** rot, float ** tmp, bool** water, bool** footpaths, bool** caves)
{
	ifstream myfile = ifstream(fn);
	if (myfile.is_open())
	{
		string line;
		w = parseInt(readLine(&myfile));
		h = parseInt(readLine(&myfile));
		//now that I have the size get me the types
	    parseIntMap(readLine(&myfile), types, w, h);  //types
		parseIntMap(readLine(&myfile), stypes, w, h); //stypes
		parseIntMap(readLine(&myfile), heights, w, h);//heightmap
		parseIntMap(readLine(&myfile), rot, w, h);//rotation
		for (int x = 0; x < w; x++)
		{
			for (int y = 0; y < h; y++)
			{
				tmp[x][y] = parseFloat(readLine(&myfile));
			}
		}
		string blank = readLine(&myfile);
		parseBoolMap(readLine(&myfile), water, w, h);//water
		parseBoolMap(readLine(&myfile), footpaths, w, h);//footpaths
		parseBoolMap(readLine(&myfile), caves, w, h);//caves
		myfile.close();
	}
	else
	{
		w = 0;
		h = 0;
	}
}

void mapio::write2DMap(ofstream * myfile, int w, int h, int ** map)
{
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			if (under10(map[x][y]))
				*myfile << "00" << map[x][y];
			else if (under100(map[x][y]))
				*myfile << "0" << map[x][y];
			else
				*myfile << map[x][y];
		}
	}
	*myfile << "\n";
}

void mapio::write2DMap(ofstream * myfile, int w, int h, bool ** map)
{
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			if (map[x][y])
				*myfile << 1;
			else
				*myfile << 0;
		}
	}
	*myfile << "\n";
}

void mapio::write2DMap(ofstream * myfile, int w, int h, float ** map)
{
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			*myfile << map[x][y];
			*myfile << "\n";
		}
	}
	*myfile << "\n";
}

string mapio::readLine(ifstream * myfile)
{
	string line;
	if (getline(*myfile, line))
		return line;
	else
		return "";
}

bool mapio::under10(int val)
{
	if (val < 10)
		return true;
	else
		return false;
}

bool mapio::under100(int val)
{
	if (val < 100)
		return true;
	else
		return false;
}

int mapio::parseInt(string val)
{
	return stoi(val);
}

float mapio::parseFloat(string val)
{
	return stof(val);
}

bool mapio::parseBool(string val)
{
	if (val == "0")
		return false;
	return true;
}

void mapio::parseIntMap(string line, int ** map, int w, int h)
{
	int i = 0;
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			string sub = line.substr(i, 3);
			map[x][y] = parseInt(sub);
			i += 3;
		}
	}
}

void mapio::parseBoolMap(string line, bool ** map, int w, int h)
{
	int i = 0;
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			string sub = line.substr(i, 1);
			map[x][y] = parseBool(sub);
			i += 1;
		}
	}
}

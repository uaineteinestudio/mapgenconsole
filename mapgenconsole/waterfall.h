#pragma once

#include "maplen.h"

class waterfall
{
public:
	waterfall(int xi, int yi, bool falloffDirs[rotLen]);
	~waterfall();
	int getX();
	int getY();
private:
	int x;
	int y;
	bool dirs[rotLen];
};


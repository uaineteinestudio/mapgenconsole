#pragma once

#include "featuregen.h"
#include "coord.h"
#include "boundchecker.h"

class relicsitegen : public featuregen
{
public:
	relicsitegen(int width, int height) : featuregen(width, height, 2)
	{
		overrideMap();
	}
	void overrideMap(); //override with new map
};


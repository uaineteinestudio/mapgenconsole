#pragma once

#include "dynInitaliser.h"
#include "chunk.h"
#include "caveGenerator.h"
#include "heightmapGenerator.h"
#include "foliagegen.h"
#include "blockTypes.h"
#include "waterGenerator.h"
#include "waterfallGenerator.h"
#include "Temp.h"
#include "maplen.h"
#include "ui.h"
#include "quickgrid.h"
#include "footpathgen.h"
#include "waterfall.h"
#include "coord.h"
#include "ChunkHandler.h"
#include <vector>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "mapio.h"		//write to file windows

class levelmap
{
public:
	levelmap();
	levelmap(int lx, int ly, bool print);
	~levelmap();
	void FillBorder();	//called in generatemap but could be called again if wanted
	void finaliseTemp();//same as this
	void findWalkable();//same as this
	void GenerateMap();	//main method to generate the whole map
	void printMaps();	//UI feature
	void destroy();		//deconstruct map to make way for another

	//get map dimensions
	int getlenx();
	int getleny();
	void getlen(int &x, int &y);//get both values

	//AM I BUSY?
	bool isbusy();

	//GET TILE TYPES
	int type(int x, int y);
	int stype(int x, int y);

	//get and set walkability
	bool isWalkable(int x, int y);
	void setWalkable(int x, int y, bool walk);

	//GET AND SET THE TEMP
	float getTemp(int x, int y);
	void setTemp(int x, int y, int tmp);

	//HEIGHTS
	int height(int x, int y);
	void setHeight(int x, int y, int h);

	//ROTATION
	int getRotation(int x, int y);
	void setRotation(int x, int y, int r);

	//CHANGE MY TYPES
	void addToSurface(int x, int y, int newSurfType);//add this to the surface layer at pos (x,y)
	void changeType(int x, int y, int newType);

	//GET SCALE OF BLOCK
	float getZScale(int type);
	float getZScale(int x, int y);

	//spawn offset
	void setoffset(float off);
	float getoffset();
	//modification offset
	float getSpawnOffset(int type);

	//discovery and visibility
	void discover(vector<coord> discv, int num);//discovered these points
	bool getDiscovered(int x, int y);
	void discover(int x, int y);
	bool getVisible(int x, int y);
	void setVisible(int x, int y);

	//selected block
	void updateSelectedBlock(int x, int y);
	void getSelectedBlock(int &x, int &y);
	void updatemousepos(int x, int y);
	void getmousepos(int &x, int &y);
	void getmouseOldpos(int &x, int &y);

	//save and load
	void savemap();
	void loadmap();
private:
	//map version
	int version = 3;
	//print info/ui
	bool printinfo = false;

	//measurements
	int lenx;
	int leny;

	//generating-busy bools
	bool generating = false;
	bool loading = false;

	//spawn offset
	float offset = 271;
	//boundary height
	int boundaryH = 300;

	//DYNAMIC MAP ARRAYS FOR TILE DAT
	bool initisedArrs = false;//initalised arrays?
	//
	int** blockTypes = 0;
	int** surfaceTypes = 0;
	int** blockHeights = 0;
	bool** water = false;
	float** temp = 0;
	bool** footpaths = false;
	bool** walkable = false;
	bool** Caves = false;
	bool** discovered = false;	//fog of war
	bool** visible = false;		//fog of war
	vector<waterfall> waterfalls;
	int** rot = 0;					//rotator of tile-has 6 edges so ranges 0 to 6
	static const int maxRot = rotLen;//		---
	static const int minRot = 0;	//	   /   \
									//	   \   /
	bool openTreasure(int xi, int yi);//	---
	bool isValidMove(int x, int y); //returns moveability of position
	void initialisearrs();			//initalise arrays
	void deletearrs();				//delete my arrays :'(
	void makeHeightMap();
	int heightmapsmoothfac = 3;
	void makeCaves();
	void makegrassPatches(int &amountLGrass, int &amountHGrass); //return the sizes here
	void makeWater();
	void makeWaterfalls();
	void makeSand(waterGenerator* wg);
	void makeFoliage();
	void InitaliseRotation();
	void InitialiseTypes();
	void establishFootpaths();		//use footpathgen to make some footpaths that 'walkable'

	//selected position
	coord selectedpos; //selected position here
	coord mousepos; //mouse position here
	coord mouseposOld; //mouse position old, updated everytime the mousepos is

	//CHUNKS
	ChunkHandler cH; //don't forget to initalise
	int activechunksradius = 2;

	//get a bool map of anytype
	bool** getMapOfType(int t, int w, int l); //don't forget to delete after please

	//UI PRINTING TO CONSOLE
	void printWaterfalls(int size);
	void printScaleMap();
};


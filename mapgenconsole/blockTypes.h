#pragma once

static const int nullBlock = -1;
static const int NoTiles = 11;
static const int NoSurfaceTiles = 14;
//in groups of 5
static bool walkableBase[NoTiles] = { 
	true, false, true, true, false, 
	false, true, true, true, true,
	true
};

static bool walkableSurface[NoSurfaceTiles] = {
	true, true, true, true, true,
	true, false, true, true, false,
	false, false, false, false
};

static float blockScales[NoTiles] = {
	1, 4, 1, 1, 3,
	1, 1, 1.05, 1, 1,
	1
};

static float spawnOffset[NoTiles] = {
	0, 5, 0, 0, 50,
	0, 5, 0, 1, 0,
	0
};

static class blockTypes
{
public:
	static const int stone = 0;
	static const int boundary = 1;
	static const int grass = 2;
	static const int sand = 3;
	static const int cave = 4;
	static const int water = 5;
	static const int footpath = 6;
	static const int caveInterior = 7;
	static const int sandgrass = 8;
	static const int lightgrass = 9;
	static const int heavygrass = 10;
	static bool isWalkable(int type);
	static float getZScale(int type);
	static float getSpawnOffset(int type);
	static bool isGrass(int type);
	static bool foliageCanGrow(int type);
};

static class surfaceTypes
{
public:
	static const int air = 0;
	static const int chest = 1;
	static const int collectedChest = 2;
	static const int waterfall = 3;
	static const int foliage = 4;
	static const int tree = 5;
	static const int Bigtree = 6;
	static const int rockgrain = 7;
	static const int rocksmall = 8;
	static const int rocklarge = 9;
	static const int colmn1 = 10;
	static const int colmn2 = 11;
	static const int colmn3 = 12;
	static const int colmn4 = 13;
	static bool isWalkable(int type);
};


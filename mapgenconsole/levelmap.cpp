#include "levelmap.h"

using namespace std;

levelmap::levelmap()
{
	lenx = MaxLen;
	leny = MaxLen;
	initialisearrs();
	InitialiseTypes();
	InitaliseRotation();
}

levelmap::levelmap(int lx, int ly, bool print)
{
	lenx = lx;
	leny = ly;
	printinfo = print;
	initialisearrs();
	InitialiseTypes();
	InitaliseRotation();
}

levelmap::~levelmap()
{
}

void levelmap::FillBorder()
{
	//fill border
	for (int i = 0; i < lenx; i++)
	{
		blockTypes[0][i] = blockTypes::boundary;
		blockHeights[0][i] = boundaryH;
		blockTypes[lenx - 1][i] = blockTypes::boundary;
		blockHeights[lenx - 1][i] = boundaryH;
	}
	for (int i = 0; i < leny; i++)
	{
		blockTypes[i][0] = blockTypes::boundary;
		blockHeights[i][0] = boundaryH;
		blockTypes[i][leny - 1] = blockTypes::boundary;
		blockHeights[i][leny - 1] = boundaryH;
	}
}

void levelmap::finaliseTemp()
{
	int sf = 2;
	tempCalc tc = tempCalc(MaxLen, sf);
	tc.calctemp(temp, blockTypes, blockHeights);
}

void levelmap::findWalkable()
{
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			if (blockTypes::isWalkable(blockTypes[x][y])) //base is walkable
			{
				if (surfaceTypes::isWalkable(surfaceTypes[x][y]))//top is walkable
					walkable[x][y] = true;
				else
					walkable[x][y] = false;
			}
			else
				walkable[x][y] = false;
		}
	}
}

//Steps for map gen
//heightmap first
//types
//water
void levelmap::GenerateMap()
{
	generating = true;
	makeHeightMap();
	InitialiseTypes();
	InitaliseRotation();
	int lG = 0;
	int hG = 0;
	makegrassPatches(lG, hG);//returns the sizes of grasses
	makeCaves();
	makeWater();
	makeWaterfalls();
	//makeSand();-called from makeWater
	finaliseTemp();
	makeFoliage();
	findWalkable();
	establishFootpaths();
	FillBorder();
	findWalkable();
	generating = false;
}

void levelmap::printMaps()
{
	if (printinfo)
	{
		
		//heightmap
		//ui::findColours();
		ui::setConsoleColour(ui::Green);
		cout << "Heights:" << endl;
		ui::print_matrix(blockHeights, lenx, leny);
		//water
		ui::setConsoleColour(ui::Blue);
		cout << "Water:" << endl;
		ui::print_matrix(water, lenx, leny);
		//types
		ui::setConsoleColour(ui::Orange);
		cout << "Types:" << endl;
		ui::print_matrix(blockTypes, lenx, leny);
		//temp
		ui::setConsoleColour(ui::Red);
		cout << "Temperature:" << endl;
		ui::print_matrix(temp, lenx, leny);
		//footpaths
		ui::setConsoleColour(ui::Purple);
		cout << "Footpaths:" << endl;
		ui::print_matrix(footpaths, lenx, leny);
		//walkable
		ui::setConsoleColour(ui::Green);
		cout << "Walkable:" << endl;
		ui::print_matrix(walkable, lenx, leny);
		//rotation
		ui::setConsoleColour(ui::Blue);
		cout << "Rotation:" << endl;
		ui::print_matrix(rot, lenx, leny);
		//surface tiles
		ui::setConsoleColour(ui::Orange);
		cout << "Surface Tiles:" << endl;
		ui::print_matrix(surfaceTypes, lenx, leny);
		//caves
		ui::setConsoleColour(ui::Red);
		cout << "Caves:" << endl;
		ui::print_matrix(Caves, lenx, leny);
		//printing the scale factors
		ui::setConsoleColour(ui::Purple);
		printScaleMap();
		ui::defColour();
		//give me the active chunks
		cH.printActiveChunks(selectedpos);
		
	}
}

void levelmap::destroy()
{
	deletearrs();
}

int levelmap::getlenx()
{
	return lenx;
}

int levelmap::getleny()
{
	return leny;
}

void levelmap::getlen(int & x, int & y)
{
	x = lenx;
	y = leny;
}

bool levelmap::isbusy()
{
	if (generating | loading)
		return true;
	else return false;
}

int levelmap::type(int x, int y)
{
	return blockTypes[x][y];
}

int levelmap::stype(int x, int y)
{
	return surfaceTypes[x][y];
}

bool levelmap::isWalkable(int x, int y)
{
	return walkable[x][y];
}

void levelmap::setWalkable(int x, int y, bool walk)
{
	walkable[x][y] = walkable;
}

float levelmap::getTemp(int x, int y)
{
	return temp[x][y];
}

void levelmap::setTemp(int x, int y, int tmp)
{
	temp[x][y] = tmp;
}

int levelmap::height(int x, int y)
{
	return blockHeights[x][y];
}

void levelmap::setHeight(int x, int y, int h)
{
	blockHeights[x][y] = h;
}

int levelmap::getRotation(int x, int y)
{
	return rot[x][y];
}

void levelmap::setRotation(int x, int y, int r)
{
	rot[x][y] = r;
}

void levelmap::addToSurface(int x, int y, int newSurfType)
{
	surfaceTypes[x][y] = newSurfType;
}

void levelmap::changeType(int x, int y, int newType)
{
	blockTypes[x][y] = newType;
}

float levelmap::getZScale(int type)
{
	return blockTypes::getZScale(type);
}

float levelmap::getZScale(int x, int y)
{
	return getZScale(type(x, y));
}

void levelmap::initialisearrs()
{
	blockTypes = dynInitaliser::makeInt(MaxLen, MaxLen, blockTypes::stone);
	surfaceTypes = dynInitaliser::makeInt(MaxLen, MaxLen, surfaceTypes::air);
	blockHeights = dynInitaliser::makeInt(MaxLen, MaxLen, 0);
	water = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	temp = dynInitaliser::makeFloat(MaxLen, MaxLen, 0);
	walkable = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	footpaths = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	rot = dynInitaliser::makeInt(MaxLen, MaxLen, minRot);
	Caves = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	//visiblity and such
	discovered = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	visible = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	//CHUNKS
	cH = ChunkHandler(lenx, leny, activechunksradius);
	//selected pos at 0 0 to start
	selectedpos = coord(0, 0);
	mousepos = coord(0, 0); 
	//as a result of having something selected as in above, we need an active chunk
	cH.UpdateActiveChunks(selectedpos, selectedpos); //old is the same as the new here
	//el fino
	initisedArrs = true;
}

void levelmap::deletearrs()
{
	dynInitaliser::del(blockTypes, MaxLen);
	dynInitaliser::del(surfaceTypes, MaxLen);
	dynInitaliser::del(blockHeights, MaxLen);
	dynInitaliser::del(water, MaxLen);
	dynInitaliser::del(temp, MaxLen);
	dynInitaliser::del(walkable, MaxLen);
	dynInitaliser::del(footpaths, MaxLen);
	dynInitaliser::del(rot, MaxLen);
	dynInitaliser::del(Caves, MaxLen);
	//visiblity and such
	dynInitaliser::del(discovered, MaxLen);
	dynInitaliser::del(visible, MaxLen);
	//el fino
	initisedArrs = false;//reset
}

void levelmap::makeHeightMap()
{
	dynInitaliser::del(blockHeights, MaxLen);
	heightmapGenerator hG = heightmapGenerator(MaxLen, MaxLen, heightmapsmoothfac);
	blockHeights = hG.genNewMap();
}

void levelmap::makeCaves()
{
	//make my caves here
	caveGenerator cg = caveGenerator(blockTypes::caveInterior);//make CAchest constructor
	dynInitaliser::del(Caves, MaxLen);
	Caves = cg.makeCaves(MaxLen, MaxLen);
	cg.purgeCaves(Caves, lenx, leny);
	//if a cave make types change
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			if (Caves[x][y])//if a cave
				blockTypes[x][y] = blockTypes::cave;
		}
	}
	//now fill interior
	cg.setInteriorCellsHex(Caves, blockTypes, blockHeights, MaxLen, MaxLen);
	cg.PlaceChests(Caves, surfaceTypes, lenx, leny);
}

void levelmap::makegrassPatches(int &amountLGrass, int &amountHGrass)
{
	//todo make heavy and light grass a thing
	//Could use CA?
	//Need to make that clustering yeah
	CA lg = CA(0.6, 5, 4, 5);
	bool** lightgrass = lg.cellautomata(lenx, leny);
	int type = 0;
	amountLGrass = 0;
	for (int x = 0; x < lenx; x++)
	{ 
		for (int y = 0; y < leny; y++)
		{
			type = blockTypes[x][y];
			if (lightgrass[x][y]) //is grass
			{
				amountLGrass += 1;
				if (blockTypes::isGrass(type))
				{
					blockTypes[x][y] = blockTypes::lightgrass;
				}
			}
		}
	}

	bool** heavygrass = lg.cellautomata(lenx, leny);
	amountHGrass = 0;
	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			type = blockTypes[x][y];
			if (heavygrass[x][y]) //is heavy grass
			{
				if (!lightgrass[x][y]) //is not light grass
				{
					amountHGrass += 1;
					if (blockTypes::isGrass(type))
					{
						blockTypes[x][y] = blockTypes::heavygrass;
					}
				}
			}
		}
	}
	//cleanup
	dynInitaliser::del(lightgrass, lenx);
	dynInitaliser::del(heavygrass, lenx);
}

void levelmap::makeWater()
{
	waterGenerator wg = waterGenerator(190);
	wg.getWater(water, blockHeights);//returns the water arr
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			if (water[x][y] && Caves[x][y] == false) //no caves allowed
				blockTypes[x][y] = blockTypes::water;
		}
	}
	makeSand(&wg);
}

void levelmap::makeWaterfalls()
{
	waterfallGenerator wg = waterfallGenerator(lenx, leny);
	int size = wg.findWaterfalls(waterfalls, blockTypes, water);
	//printWaterfalls(size);
	int x = 0;
	int y = 0;
	for (int i = 0; i < size; i++)
	{
		x = waterfalls[i].getX();
		y = waterfalls[i].getY();
		surfaceTypes[x][y] = surfaceTypes::waterfall;
	}
}

void levelmap::makeSand(waterGenerator* wg)
{
	int size = 0;
	vector<coord> bounds = wg->getBoundaries(water, size);
	for (int i = 0; i < size; i++)
	{
		int xi = bounds[i].x;
		int yi = bounds[i].y;
		switch (blockTypes[xi][yi])
		{
		case blockTypes::cave:
			//do nothing
			break;
		default:
			blockTypes[xi][yi] = blockTypes::sand;
		}
	}
	//cleanup please
	bounds.clear();
	bounds = vector<coord>();

	//now that I have 'sand' get the boundaries of sand and grass please
	bool** sandmap = getMapOfType(blockTypes::sand, MaxLen, MaxLen);
	size = 0;
	bounds = wg->getBoundaries(sandmap, size);
	for (int i = 0; i < size; i++)
	{
		if (blockTypes[bounds[i].x][bounds[i].y] == blockTypes::grass) //sand and grass bordering
			blockTypes[bounds[i].x][bounds[i].y] = blockTypes::sandgrass;
	}
	//cleanup please
	bounds.clear();
	bounds = vector<coord>();
	dynInitaliser::del(sandmap, MaxLen);

	bool** sandmap2 = getMapOfType(blockTypes::sandgrass, MaxLen, MaxLen);
	size = 0;
	bounds = wg->getBoundaries(sandmap2, size);
	for (int i = 0; i < size; i++)
	{
		if (blockTypes[bounds[i].x][bounds[i].y] == blockTypes::grass) //sand and grass bordering
			blockTypes[bounds[i].x][bounds[i].y] = blockTypes::sandgrass;
	}
	//cleanup please
	dynInitaliser::del(sandmap2, MaxLen);
}

void levelmap::makeFoliage()
{
	foliagegen fg = foliagegen(lenx, leny);
	fg.generateFoliage(blockTypes, surfaceTypes, temp);
	//now to see what that produces
}

void levelmap::InitaliseRotation()
{
	/* initialize random seed: */
	srand(time(NULL));
	int range = maxRot - minRot;
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			rot[x][y] = (rand() % range) + minRot;
		}
	}
}

void levelmap::InitialiseTypes()
{
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			blockTypes[x][y] = blockTypes::grass;
			surfaceTypes[x][y] = surfaceTypes::air;
		}
	}
}

void levelmap::establishFootpaths()
{
	footpathgen fg = footpathgen(lenx, leny);
	fg.establishFootpaths(footpaths, walkable, blockTypes);
}

bool ** levelmap::getMapOfType(int t, int w, int l)
{
	bool** map = dynInitaliser::makeBool(w, l, false); //intialise
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < l; y++)
		{
			if (blockTypes[x][y] == t)
				map[x][y] = true;
		}
	}
	return map;

}

void levelmap::printWaterfalls(int size)
{
	if (printinfo)
	{
		
		//now print
		int** blankArr = dynInitaliser::makeInt(lenx, leny, 0);
		int x = 0;
		int y = 0;
		for (int i = 0; i < size; i++)
		{
			x = waterfalls[i].getX();
			y = waterfalls[i].getY();
			cout << x << " " << y << endl;
			blankArr[x][y] = 1;
		}
		ui::print_matrix(blankArr, lenx, leny);
		dynInitaliser::del(blankArr, lenx);
		
	}
}

void levelmap::printScaleMap()
{
	
	float** zscale = dynInitaliser::makeFloat(lenx, leny, 1); //initalised with a value of 1
	int type = 0;
	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			type = blockTypes[x][y];
			zscale[x][y] = blockTypes::getZScale(type);//from type
		}
	}
	cout << "Z Scale factor:" << endl;
	ui::print_matrix(zscale, lenx, leny);
	//clean up by deleting
	dynInitaliser::del(zscale, lenx);
	
}

bool levelmap::openTreasure(int xi, int yi)
{
	if (surfaceTypes[xi][yi] == surfaceTypes::chest)
	{
		surfaceTypes[xi][yi] = surfaceTypes::collectedChest;
		return true;
	}
	return false;
}

bool levelmap::isValidMove(int x, int y)
{
	return blockTypes::isWalkable(blockTypes[x][y]);
}

void levelmap::setoffset(float off)
{
	offset = off;
}

float levelmap::getoffset()
{
	return offset;
}

float levelmap::getSpawnOffset(int type)
{
	return blockTypes::getSpawnOffset(type);
}

void levelmap::discover(vector<coord> discv, int num)
{
	for (int i = 0; i < num; i++)
	{
		discovered[discv[i].x][discv[i].y] = true;
	}
}

bool levelmap::getDiscovered(int x, int y)
{
	return discovered[x][y];
}

void levelmap::discover(int x, int y)
{
	discovered[x][y] = true;
}

bool levelmap::getVisible(int x, int y)
{
	return visible[x][y];
}

void levelmap::setVisible(int x, int y)
{
	visible[x][y] = true;
	if (getDiscovered(x, y) == false)
		discover(x, y);
}

void levelmap::updateSelectedBlock(int x, int y)
{
	coord oldpos = selectedpos;
	selectedpos.x = x;
	selectedpos.y = y;
	cH.UpdateActiveChunks(selectedpos, oldpos);
}

void levelmap::getSelectedBlock(int & x, int & y)
{
	x = selectedpos.x;
	y = selectedpos.y;
}

void levelmap::updatemousepos(int x, int y)
{
	mouseposOld = mousepos;
	mousepos.x = x;
	mousepos.y = y;
}

void levelmap::getmousepos(int & x, int & y)
{
	x = mousepos.x;
	y = mousepos.y;
}

void levelmap::getmouseOldpos(int & x, int & y)
{
	x = mouseposOld.x;
	y = mouseposOld.y;
}

void levelmap::savemap()
{
	mapio::writeFile("testout.txt", lenx, leny, blockTypes, surfaceTypes, blockHeights, rot, temp, water, footpaths, Caves);
}

void levelmap::loadmap()
{
	loading = true;
	mapio::loadFile("testout.txt", lenx, leny, blockTypes, surfaceTypes, blockHeights, rot, temp, water, footpaths, Caves);
	//CHUNKS
	cH = ChunkHandler(lenx, leny, activechunksradius);
	cH.UpdateActiveChunks(selectedpos, selectedpos);
	findWalkable();
	loading = false;
}

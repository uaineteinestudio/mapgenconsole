#include "caveGenerator.h"

caveGenerator::caveGenerator()
{
	Interior = 0;
}

caveGenerator::~caveGenerator()
{
}

bool ** caveGenerator::makeCaves(int width, int height)
{
	bool ** caves = cellautomata(width, height);
	//ui::print_matrix(caves, 20, 20); //this is for debug
	//purgeCaves(caves, width, height); //now to be called at a higher level being public
	return caves;
}

void caveGenerator::setInteriorCells(bool ** cavemap, int ** blocktypes, int width, int height)
{
	int size = 0;
	int xi = 0;
	int yi = 0;
	vector<coord> cells = interiorCells(cavemap, width, height, size);
	for (int i = 0; i < size; i++)
	{
		xi = cells[i].x;
		yi = cells[i].y;
		blocktypes[xi][yi] = Interior;
	}
}

void caveGenerator::setInteriorCellsHex(bool ** cavemap, int ** blocktypes, int** blockheights, int width, int height)
{
	int size = 0;
	int xi = 0;
	int yi = 0;
	int heightsum = 0;
	vector<coord> cells = interiorCellsHex(cavemap, width, height, size);
	for (int i = 0; i < size; i++)
	{
		xi = cells[i].x;
		yi = cells[i].y;
		blocktypes[xi][yi] = Interior;
		heightsum += blockheights[xi][yi];
	}
	int average = heightsum / size;
	for (int i = 0; i < size; i++)
	{
		xi = cells[i].x;
		yi = cells[i].y;
		blockheights[xi][yi] = average; //average height set
	}
}

vector<coord> caveGenerator::interiorCells(bool ** caves, int width, int height, int &size)
{
	size = 0;
	vector<coord> cells = vector<coord>();
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			if (isInterior(caves, x, y, width, height))
			{
				size += 1;
				cells.push_back(coord(x, y));
			}
		}
	}
	return cells;
}

vector<coord> caveGenerator::interiorCellsHex(bool ** caves, int width, int height, int &size)
{
	size = 0;
	vector<coord> cells = vector<coord>();
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			if (isInteriorHex(caves, x, y, width, height))
			{
				size += 1;
				cells.push_back(coord(x, y));
			}
		}
	}
	return cells;
}

void caveGenerator::PlaceChests(bool ** cavemap, int ** surfacetypes, int width, int height)
{
	int size = 0;
	int xi = 0;
	int yi = 0;
	vector<coord> finalChests = GetTreasureSpots(cavemap, width, height, size);
	for (int i = 0; i < size; i++)
	{
		xi = finalChests[i].x;
		yi = finalChests[i].y;
		surfacetypes[xi][yi] = surfaceTypes::chest;
	}
}

bool caveGenerator::isInterior(bool ** map, int xi, int yi, int width, int height)
{
	int col[4] = { -1, 0, 0, 1 };
	int row[4] = { 0, -1, 1, 0 };

	int x = 0;
	int y = 0;

	if (map[xi][yi] == false)//if a cave required
	{
		return false;
	}
	else
	{
		for (int i = 0; i < 4; i++)
		{
			x = xi + col[i];
			y = yi + row[i];
			if (boundchecker::inBounds(x, y, width, height))
				if (map[x][y] == false)
					return false;
		}
		return true; //else
	}
}

bool caveGenerator::isInteriorHex(bool ** map, int xi, int yi, int width, int height)
{
	int x = 0;
	int y = 0;
	int sum = 0;

	int col[HexAdjLen];
	int row[HexAdjLen];
	hexAdjaceny::WriteColRow(col, row, xi, yi);

	if (map[xi][yi] == false)//if a cave required
	{
		return false;
	}
	else //is a cave
	{
		for (int i = 0; i < HexAdjLen; i++)
		{
			x = xi + col[i];
			y = yi + row[i];
			if (boundchecker::inBounds(x, y, width, height))
			{
				if (map[x][y] == false) //is not a cave
					return false; //we are already having to stop as this is not interior
				else
					sum += 1;
			}
		}
		if (sum == HexAdjLen)
			return true; //else when all 6 are caves
	}
	return false; //all else
}

void caveGenerator::purgeCaves(bool ** caves, int w, int h)
{
	floodfillersize fs = floodfillersize(w, h);
	int** sizeArr = dynInitaliser::makeInt(w, h, 0);
	fs.getSizeArray(caves, sizeArr, w, h);
	//ui::print_matrix(sizeArr, w, h);
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			if (sizeArr[x][y] < purgeSmallerThan)
				caves[x][y] = false;
			else if (sizeArr[x][y] > purgeLargerThan)
				caves[x][y] = false;
		}
	}
	//ui::print_matrix(caves, w, h); //this is for debug
	if (!HasCave(caves, w, h)) //if false damn
	{
		makeArtificalCave(caves, w, h);
	}
}

void caveGenerator::DebugPrintStats(float p)
{
	cout << p << "% of cave gen complete" << endl;
}

void caveGenerator::makeArtificalCave(bool ** cavesmap, int w, int h)
{
	int select = rand() % numArtCaves;
	int x0 = rand() % (w - artCaveDimLen);
	int y0 = rand() % (h - artCaveDimLen);
	for (int x = 0; x < artCaveDimLen; x++)
	{
		for (int y = 0; y < artCaveDimLen; y++)
		{
			cavesmap[x0 + x][y0 + y] = artCavs[select][x][y];
		}
	}
}

bool caveGenerator::HasCave(bool ** cavemapAfterPurge, int width, int height)
{
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			if (cavemapAfterPurge[x][y]) //is true
				return true;
		}
	}
	//else not one found
	return false;
}

bool caveGenerator::oppval(bool ** caves, int x, int y)
{
	if (caves[x][y])
		return false;
	return true;
}

float caveGenerator::calcPercComplete(int num, int max)
{
	return (float)num / float(max) * 100;
}

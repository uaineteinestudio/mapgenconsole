#pragma once

#include "piece.h"
#include "coord.h"
#include <vector>

using namespace std;

static const int numTypes = 5;

static class defpieces
{
public:
	static vector<piece> getDefaultPieces(int belongsTo);
};

class wizard : public piece
{
public:
	wizard(coord pos, int belongsTo) : piece(pos, belongsTo, 0)
	{
		//extra
	}
};

class squire : public piece
{
public:
	squire(coord pos, int belongsTo) : piece(pos, belongsTo, 1)
	{
		//extra
	}
};

class hero : public piece
{
public:
	hero(coord pos, int belongsTo) : piece(pos, belongsTo, 2)
	{
		//extra
	}
};

class healer : public piece
{
public:
	healer(coord pos, int belongsTo) : piece(pos, belongsTo, 3)
	{
		//extra
	}
};

class rouge : public piece
{
public:
	rouge(coord pos, int belongsTo) : piece(pos, belongsTo, 4)
	{
		//extra
	}
};

#pragma once

#include "genclass.h"
#include "cellautoma.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

using namespace std;

class foliagegen : public genclass
{
public:
	foliagegen() : genclass()
	{
		foliageTempLim = 0.6;
		chanceTree = 0.15;
	}
	foliagegen(int width, int height) : genclass(width, height)
	{
		foliageTempLim = 0.6;
		chanceTree = 0.15;
	}
	foliagegen(int width, int height, float ftL, float chanceT, float chanceFoliage) : genclass(width, height)
	{
		foliageTempLim = ftL;
		chanceTree = chanceT;
		foliageChance = chanceFoliage;
	}
	~foliagegen();
	void generateFoliage(int** blocktypes, int** surfacetypes, float** temp);//surface types returned
private:
	void makeTree(int xi, int yi, int** surfaceMap);//make tree at this point
	void makeFoliage(int xi, int yi, int** surfaceMap);
	float foliageTempLim;
	float chanceBigTree = 0.5; //big tree
	float foliageChance = 0.1; //half the amount of foliage normally thanks
	float chanceTree;
};


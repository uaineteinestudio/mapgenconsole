#include "defpieces.h"

using namespace std;

vector<piece> defpieces::getDefaultPieces(int belongsTo)
{
	vector<piece> thepieces;
	thepieces.push_back(hero(coord(0, 0), belongsTo));
	thepieces.push_back(wizard(coord(0, 0), belongsTo));
	thepieces.push_back(rouge(coord(0, 0), belongsTo));
	thepieces.push_back(healer(coord(0, 0), belongsTo));
	thepieces.push_back(squire(coord(0, 0), belongsTo));
	return thepieces;
}

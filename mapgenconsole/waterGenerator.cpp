#include "waterGenerator.h"

waterGenerator::waterGenerator(int hL)
{
	heightLim = hL;
}

waterGenerator::~waterGenerator()
{
}

void waterGenerator::getWater(bool ** newmap, int ** heightmap)
{
	int i = 0;
	int originalHL = heightLim;
	int acceptableWaterSize = 20;
	while (i < attemptLimit)
	{
		for (int x = 0; x < MaxLen; x++)
		{
			for (int y = 0; y < MaxLen; y++)
			{
				newmap[x][y] = belowLim(heightmap[x][y]);
			}
		}
		//now count
		int size = howMuchWater(newmap);
		if (size < acceptableWaterSize)
			heightLim += 3;
		else
			break;
		i += 1;
	}
	//restore
	heightLim = originalHL;
}

bool waterGenerator::belowLim(int atHeight)
{
	if (atHeight < heightLim)
		return true;
	else
		return false;
}

vector<coord> waterGenerator::getBoundaries(bool ** water, int &size)
{
	/* initialize random seed: */
	srand(time(NULL));
	float r = 0;

	size = 0;
	vector<coord> boundaries = vector<coord>();
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			if (r < sandchance)
			{
				if (isBoundary(water, x, y))
				{
					size += 1;
					boundaries.push_back(coord(x, y));
				}
			}
		}
	}
	return boundaries;
}

int waterGenerator::getHeightLimit()
{
	return heightLim;
}

void waterGenerator::setnewLim(int newlim)
{
	heightLim = newlim;
}

int waterGenerator::howMuchWater(bool ** watermap)
{
	int size = 0;
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			if (watermap[x][y])
			{
				size += 1;
			}
		}
	}
	return size;
}

bool waterGenerator::isBoundary(bool ** map, int x, int y)
{
	int col[4] = { -1, 0, 0, 1 };
	int row[4] = { 0, -1, 1, 0 };
	if (map[x][y] == true)
	{
		return false;
	}
	for (int i = 0; i < 4; i++)
	{
		int xi = x + col[i];
		int yi = y + row[i];
		if (boundchecker::inBounds(xi, yi, MaxLen, MaxLen))
			if (map[xi][yi] == true)
				return true;
	}
	return false; //else
}
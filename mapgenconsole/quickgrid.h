#pragma once

#ifndef __QUICKGRID_H__
#define __QUICKGRID_H__

#include "Node.h"
#include "CompareG.h"
#include "CompareF.h"
#include "Canvas.h"
#include <vector>

#endif

using namespace std;

static class quickgrid 
{
public:
	static bool main(int m, int n, bool** arr, int sx, int sy, int ex, int ey);
	static vector<coord> mainPath(int m, int n, bool** arr, int sx, int sy, int ex, int ey, int&size);
};
#pragma once

#include "coord.h"

class boardobj //abstract class
{
public:
	boardobj(coord p);
	boardobj(int xi, int yi);
	~boardobj();
protected:
	coord pos;
};


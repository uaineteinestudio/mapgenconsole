// mapgenconsole.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "levelmap.h"
//#include "coord.h"
#include <chrono>

void testGen()
{
	int i = 0;
	while (i < 200)
	{
		int width = 20;
		int height = 20;
		levelmap map = levelmap(width, height, true);
		map.GenerateMap();
		map.printMaps();
		//cout << map.calcDist(coord(0, 0), coord(5, 10)) << endl;  //#include "coord.h"
		map.destroy();
		system("PAUSE");
		i += 1;
	}
}

void testChunkUpdate()
{
	int i = 0;
	while (i < 200)
	{
		int width = 100;
		int height = 100;
		levelmap map = levelmap(width, height, true);
		map.GenerateMap();
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
		map.updateSelectedBlock(18, 14);
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[micros]" << std::endl;
		map.updateSelectedBlock(2, 2);
		map.printMaps();
		map.destroy();
		system("PAUSE");
		i += 1;
	}
}

void testSave()
{
	int i = 0;
	//while (i < 200)
	//{
		int width = 20;
		int height = 20;
		levelmap map = levelmap(width, height, true);
		map.GenerateMap();
		map.updateSelectedBlock(18, 14);
		map.updateSelectedBlock(2, 2);
		map.printMaps();
		map.savemap();
		map.destroy();
		system("PAUSE");
		i += 1;
	//}
}

void testLoad()
{
	int i = 0;
	//while (i < 200)
	//{
		levelmap map = levelmap(MaxLen, MaxLen, true);
		map.loadmap();
		map.printMaps();
		map.destroy();
		system("PAUSE");
		i += 1;
	//}
}

int main()
{
    std::cout << "Hello World!\n";
	//testGen();
	//testChunkUpdate();
	testSave();
	testLoad();
}

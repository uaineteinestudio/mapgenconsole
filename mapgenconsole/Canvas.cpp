#include "Canvas.h"
#include <chrono>
#include <thread>

using namespace std;

Canvas::Canvas(int m, int n, bool** arr, int sx, int sy, int ex, int ey) 
{
	readGrid(m, n, arr, sx, sy, ex, ey);
	
	rows = grid.size();
	cols = (!grid.empty() ? grid[0].size() : 0);
	
	assignNeighbors();
}

Canvas::~Canvas()
{
	if (!grid.empty())
	{
		for (int i = 0; i < rows; i++)
		{
			if (!grid[i].empty()) 
			{
				for (int j = 0; j < cols; j++) 
				{
					delete grid[i][j];
				}
			}
		}
	}
}

void Canvas::readGrid(int m, int n, bool** arr, int sx, int sy, int ex, int ey) 
{
	//std::ifstream fin(filename);
	
	int i = 0;//row
	int j = 0;//column
	std::vector<Node*> line;
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
		{
			char c = '.';//default
			if (i == sy && j == sx)
			{
				c = 'S';
				start = std::make_pair(i, j);
			}
			else if (i == ey && j == ex)
			{
				c = 'E';
				end = std::make_pair(i, j);
			}
			else if (arr[i][j] == false)
				c = 'X';
			Node* sq = new Node(i, j, c);
			line.push_back(sq);
		}
		grid.push_back(line);
		line.clear();
	}
}

void Canvas::assignNeighbors() 
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			Node* sq = grid[i][j];

			if (i > 0) {
				if (!(grid[i-1][j])->isObstacle())
				{
					sq->addNeighbor(std::make_pair(i-1, j));
				}
			}
			if (i < rows-1)
			{
				if (!(grid[i+1][j])->isObstacle())
				{
					sq->addNeighbor(std::make_pair(i+1, j));
				}
			}
			if (j > 0)
			{
				if (!(grid[i][j-1])->isObstacle())
				{
					sq->addNeighbor(std::make_pair(i, j-1));
				}
			}		
			if (j < cols-1)
			{
				if (!(grid[i][j+1])->isObstacle())
				{
					sq->addNeighbor(std::make_pair(i, j+1));
				}
			}		
		}
	}
}


Node* Canvas::get(int i, int j) 
{
	if (i >= 0 && i < rows && j >= 0 && j < cols) {
		return grid[i][j];
	}
	else
	{
		return nullptr;
	}
}

Node* Canvas::get(std::pair<int, int> coord)
{
	return Canvas::get(coord.first, coord.second);
}

vector<coord> Canvas::getPath(int sx, int sy, int ex, int ey, int &size)
{
	size = 0;
	vector<coord> pat = vector<coord>();
	pat.push_back(coord(sx, sy));
	int yi = 0;
	for (auto& v : grid)
	{
		int xi = 0;
		for (Node* sq : v)
		{
			if (sq->getSymbol() == '*')
			{
				pat.push_back(coord(xi,yi));
				size += 1;
			}
			xi += 1;
		}
		yi += 1;
	}
	pat.push_back(coord(ex, ey));
	return pat;
}


Node* Canvas::getStart()
{
	return Canvas::get(start); 
}

Node* Canvas::getEnd()
{
	return Canvas::get(end);
}


float Canvas::getDist(Node* a, Node* b)
{
	float distX = (float) (a->getCol() - b->getCol());
	float distY = (float) (a->getRow() - b->getRow());
	return (float) (sqrt((distX*distX) + (distY*distY)));
}


void Canvas::draw()
{
	Canvas::draw(DEFAULT_INTERVAL);
}

void Canvas::draw(int nanosec)
{
	system("cls");
	for (auto& v : grid) 
	{
		for (Node* sq : v)
		{
			std::cout << sq->getSymbol();
		}
		std::cout << std::endl;
	}
	std::cout << std::flush;	
	std::this_thread::sleep_for(std::chrono::microseconds(nanosec));
}



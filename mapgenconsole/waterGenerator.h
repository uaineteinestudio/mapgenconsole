#pragma once

#include "maplen.h"
#include "coord.h"
#include "boundchecker.h"
#include <vector>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

using namespace std;

class waterGenerator
{
public:
	waterGenerator(int hL);
	~waterGenerator();
	void getWater(bool** newmap, int** heightmap);
	bool belowLim(int atHeight);
	vector<coord> getBoundaries(bool** water, int &size);
	int getHeightLimit();
	void setnewLim(int newlim);
	int howMuchWater(bool ** watermap);
private:
	int heightLim;
	bool isBoundary(bool** map, int x, int y);
	float sandchance = 0.9;
	int attemptLimit = 5; //limit on modifying the height limit for water
};


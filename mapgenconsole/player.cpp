#include "player.h"

player::player()
{
	index = -1;//needs one really
	initaliseArrs();
}

player::player(int ind)
{
	index = ind;
	initaliseArrs();
}

player::~player()
{
}

void player::initaliseArrs()
{

	initalised = true;
}

void player::delArrs()
{

	initalised = false;
}

void player::addPiece(piece addthis)
{
	NoPieces += 1;
	playerPieces.push_back(addthis);
}

int player::getRemainingCash()
{
	return movementCash;
}

void player::movePiece(int i, coord newCoord)
{
	//need to know movement length with piece
}

void player::deductMovementCash(int movementamnt)
{
	movementCash -= movementamnt;
}

bool player::canMoveThisFar(int movementamnt)
{
	if (movementCash < movementamnt)
		return false;
	else
		return true;
}

void player::EndTurn()
{
	movementCash = NormMovementcash;
}

void player::initialisepieces()
{
	playerPieces = defpieces::getDefaultPieces(index);
}

#pragma once

#include "maplen.h"
#include "cellautomachest.h"
#include "coord.h"
#include "artificialCaves.h"
#include "hexadjacency.h"
#include "blockTypes.h"
#include <iostream>
//#include "ui.h"

class caveGenerator : public CAChest //inherit
{
public:
	caveGenerator();//default
	caveGenerator(float ch, int dl, int bl, int smoofac, int tL, int nC, int interior) :
		CAChest(ch, dl, bl, smoofac, tL, nC)
	{
		//interior block type
		Interior = interior;
	}
	caveGenerator(int interior, int tL, int nC) : CAChest(tL, nC)
	{
		Interior = interior;
	}
	caveGenerator(int interior) : CAChest()
	{
		//interior block type
		Interior = interior;
	}
	~caveGenerator();
	bool** makeCaves(int width, int height);
	void purgeCaves(bool** caves, int w, int h);
	void setInteriorCells(bool** cavemap, int** blocktypes, int width, int height);
	void setInteriorCellsHex(bool** cavemap, int** blocktypes, int** blockheights, int width, int height);
	vector<coord> interiorCells(bool** caves, int width, int height, int &size);//return size too
	vector<coord> interiorCellsHex(bool** caves, int width, int height, int &size);//return size too
	void PlaceChests(bool** cavemap, int** surfacetypes, int width, int height);
private:
	bool isInterior(bool ** map, int xi, int yi, int width, int height);
	bool isInteriorHex(bool ** map, int xi, int yi, int width, int height);
	int Interior;
	//purge caves smaller than this:
	int purgeSmallerThan = 11;
	int purgeLargerThan = 44;
	bool HasCave(bool** cavemapAfterPurge, int width, int height); //has at least 1 cave on the map
	bool oppval(bool** caves, int x, int y);
	float calcPercComplete(int num, int max);
	void DebugPrintStats(float p);
	void makeArtificalCave(bool** cavesmap, int w, int h);
};


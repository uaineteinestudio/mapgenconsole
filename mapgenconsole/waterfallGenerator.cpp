#include "waterfallGenerator.h"

waterfallGenerator::waterfallGenerator(int width, int height)
{
	w = width;
	h = height;
}

waterfallGenerator::~waterfallGenerator()
{
}

int waterfallGenerator::findWaterfalls(vector<waterfall>& falls, int** blocktypes, bool ** water)
{
	falls.clear();//make sure is size 0
	int len = 0;
	bool dirs[rotLen];
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{

			if (isWaterfall(blocktypes, water, x, y, dirs))
			{
				waterfall newfall = waterfall(x, y, dirs);
				falls.push_back(newfall);
				len += 1;
			}
		}
	}
	return len;
}

bool waterfallGenerator::isWaterfall(int ** bt, bool ** water, int x, int y, bool *dirs)
{
	int col[4] = { 0, 1, 0, -1 };
	int row[4] = { -1, 0, 1, 0 };
	if (bt[x][y] != blockTypes::cave)//is a cave is necessary
	{
		return false; //isn't so we can ignore this
	}
	bool anytrue = false;
	for (int i = 0; i < 4; i++)
	{
		int xi = x + col[i];
		int yi = y + row[i];
		if (boundchecker::inBounds(xi, yi, MaxLen, MaxLen))
		{
			if (water[xi][yi] == true)//bordering water than is waterfall
			{
				anytrue = true;
				dirs[i] = true;
			}
			else
				dirs[i] = false;
		}
		else
			dirs[i] = false;
	}
	return anytrue; //else
}

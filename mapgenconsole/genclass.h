#pragma once

#include "maplen.h"
#include "boundchecker.h"
#include "dynInitaliser.h"
#include "blockTypes.h"

class genclass //abstract
{
public:
	genclass();
	genclass(int width, int height);
	~genclass();
	int getlenx();
	int getleny();
protected:
	int w;
	int h;
};


#pragma once

#include "Node.h"

class CompareF
{
public:
	bool operator() (const Node* a, const Node* b) const;
};
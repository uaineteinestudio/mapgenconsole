#include "waterfall.h"

waterfall::waterfall(int xi, int yi, bool falloffDirs[rotLen])
{
	x = xi;
	y = yi;
	for (int i = 0; i < rotLen; i++)
	{
		dirs[i] = falloffDirs[i];
	}
}

waterfall::~waterfall()
{
}

int waterfall::getX()
{
	return x;
}

int waterfall::getY()
{
	return y;
}

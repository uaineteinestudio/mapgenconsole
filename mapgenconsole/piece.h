#pragma once

#include "boardobj.h"

class piece : public boardobj
{
public:
	piece(coord p, int belongsTo, int id) : boardobj(p)
	{
		ID = id;
		owner = belongsTo;
		selected = false;
		alive = true;
	}
	piece(int xi, int yi, int belongsTo, int id) : boardobj(xi, yi)
	{
		ID = id;
		owner = belongsTo;
		selected = false;
		alive = true;
	}
	~piece();
	int getOwner();
	void kill();
protected:
	int ID;
	int owner;
	float health = 100;
	bool selected;
	bool alive;
private:
	float atckran = 1;
	float defran = 0.5;
};


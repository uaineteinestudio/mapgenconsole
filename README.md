# mapgenconsole

A c++ map generator for square tiles with types (both base and surface types), heightmap, caves (cellular automata), chest placement, water level, water edge detection, floodfiller, walkable properties, seletion capabilites, active chunks to restrict update overhead, footpath spawning and general pathfinding and IO.

## Getting Started

See the mapgenconsole.cpp file for execution of various problems and the levelmap.h and levelmap.cpp for the building of the maps.

## Authors

* **Daniel Stamer-Squair** - *UaineTeine*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.